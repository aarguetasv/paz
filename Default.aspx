﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="Paz._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

  <div id="banner" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="jumbotron">
          <h1 class="small">Bienvenido</h1>
          <p class="big">Prueba de conexion SQL para servidores informáticos</p><br />
            <asp:Button class="btn btn-banner" ID="btn_test" runat="server" Text="Realizar prueba de conexion" />
        </div>
      </div>
    </div>
  </div>
  <!--BANNER END-->

  <!--CTA1 START-->
  <div class="cta-1">
    <div class="container">
      <div class="row text-center white">
        <h1 class="cta-title">
            <asp:Label ID="lbl_status" runat="server" Text="Esperando por inicio de conexion"></asp:Label></h1>
      </div>
    </div>
  </div>
  <!--CTA1 END-->

</asp:Content>
